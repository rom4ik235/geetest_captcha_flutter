import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:geetest_captcha_flutter/geetest_captcha_flutter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  String api1 = "https://www.geetest.com/demo/gt/validate-slide";

  @override
  void initState() {
    GeetestCaptchaFlutter.initCallHandler();
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    String platformVersion;
    try {
      platformVersion = await GeetestCaptchaFlutter.platformVersion ??
          'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: TextButton(
            child: Text('Running on: $_platformVersion\n'),
            onPressed: () {
              GeetestCaptchaFlutter.initGeetest(
                  api1: api1,
                  authToken: "Token",
                  onRequest: (String result) async {
                    return true;
                  });
            },
          ),
        ),
      ),
    );
  }
}
