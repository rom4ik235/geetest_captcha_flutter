import Flutter
import UIKit

public class SwiftGeetestCaptchaFlutterPlugin: NSObject, FlutterPlugin {
    static var geetest : Geetest?
    static var channel : FlutterMethodChannel?
  public static func register(with registrar: FlutterPluginRegistrar) {
    SwiftGeetestCaptchaFlutterPlugin.channel = FlutterMethodChannel(name: "geetest_captcha_flutter", binaryMessenger: registrar.messenger())
    let instance = SwiftGeetestCaptchaFlutterPlugin()
    registrar.addMethodCallDelegate(instance, channel: SwiftGeetestCaptchaFlutterPlugin.channel!)
    
    
  }
    
  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch call.method {
    case "getPlatformVersion":
        result("iOS" + UIDevice.current.systemVersion)
    case "initGeetest":
        guard let dict = call.arguments as? Dictionary<String, Any> else {
            return
        }
        print(dict)
        if let api1 = dict["api1"] as? String  {
            if let authToken = dict["authToken"] as? String {
                SwiftGeetestCaptchaFlutterPlugin.geetest = Geetest(api_1: api1 , api_2: "",auth: authToken )
                SwiftGeetestCaptchaFlutterPlugin.geetest!.startCaptcha();
            }
        }
    default:
        print(call.method)
    }
  }
}
