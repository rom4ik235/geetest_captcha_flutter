//
//  Geetest.swift
//  geetest_captcha_flutter
//
//  Created by Roman on 02.10.2021.
//

import Foundation
import GT3Captcha

class Geetest: NSObject, GT3CaptchaManagerDelegate,GT3CaptchaManagerViewDelegate, GT3CaptchaNetworkDelegate{
    var geetest: GT3CaptchaManager?
    var authToken: String
    
    init(api_1:String ,api_2:String,auth:String ) {
        self.authToken = auth
        super.init();
        if(geetest != nil){
            geetest = nil
        }
        geetest = GT3CaptchaManager(api1: api_1, api2: api_2, timeout: TimeInterval.init(5.0))
        geetest?.customAPI2ResultShowOccasion = true
        geetest?.registerCaptcha(nil)
        geetest!.delegate = self
        geetest!.viewDelegate = self
    }
    
    public func startCaptcha(){
        geetest?.startGTCaptchaWith(animated: true);
    }
    
    func gtCaptcha(_ manager: GT3CaptchaManager, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential) -> Void) {
    }
    
    
    func gtCaptcha(_ manager: GT3CaptchaManager, errorHandler error: GT3Error) {
        if (error.code == -999) {
            print("[gtCaptcha] Error", "The request was aborted unexpectedly, usually due to a user cancellation operation.")
        }
        else if (error.code == -10) {
            print("[gtCaptcha] Error", "Locked during pre-judgment, no graphical check will be performed.")
        }
        else if (error.code == -20) {
            print("[gtCaptcha] Error", "Try too much")
        }
        else {
            print("[gtCaptcha] Error", "Network issue or parsing failure, see developer documentation for additional error codes.")
        }
    }
    
    func gtCaptcha(_ manager: GT3CaptchaManager, didReceiveSecondaryCaptchaData data: Data?, response: URLResponse?, error: GT3Error?, decisionHandler: @escaping (GT3SecondaryCaptchaPolicy) -> Void) {
        if let d:Data = data{
            print("[gtCaptcha] didReceiveSecondaryCaptchaData data", String(data: d, encoding: String.Encoding.utf8) ?? "")
        }
    }
    
    func gtCaptcha(_ manager: GT3CaptchaManager, didReceiveCaptchaCode code: String, result: [AnyHashable : Any]?, message: String?) {
        print("[gtCaptcha] didReceiveCaptchaCode data", code)
    }
    
    func gtCaptcha(_ manager: GT3CaptchaManager, willSendRequestAPI1 originalRequest: URLRequest, withReplacedHandler replacedHandler: @escaping (URLRequest) -> Void) {
        var newRequest = URLRequest(url: originalRequest.url!)
        newRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        newRequest.addValue(authToken, forHTTPHeaderField: "Authorization")
        replacedHandler(newRequest)
    }
    
    func gtCaptcha(_ manager: GT3CaptchaManager, didReceiveDataFromAPI1 dictionary: [AnyHashable : Any]?, withError error: GT3Error?) -> [AnyHashable : Any]? {
        return dictionary
    }
    
    func api2(result:Any){
        guard let answer = result as? Bool else{
            self.geetest!.closeGTViewIfIsOpen()
            return
        }

        if answer {
            self.geetest!.showGTCaptchaSuccessView()
        }
        else {
            self.geetest!.closeGTViewIfIsOpen()
        }
    }
    
    func gtCaptcha(_ manager: GT3CaptchaManager, willSendSecondaryCaptchaRequest originalRequest: URLRequest, withReplacedRequest replacedRequest: @escaping (NSMutableURLRequest) -> Void) {
        var list:[Substring] = []
        var responce_string = "{\""
        if let data:Data = originalRequest.httpBody{
            let answer = String(data: data,encoding: String.Encoding.utf8)
            list = answer!.split(separator: "&")
            for item in list {
                let keyValue = item.split(separator: "=")
                responce_string = responce_string + keyValue[0] + "\":\"" + keyValue[1] + "\",\""
            }
            responce_string = responce_string + "test\":\"test\"}"
        }else{
            responce_string = "{\"msg\":\"Fail\"}"
        }
        SwiftGeetestCaptchaFlutterPlugin.channel!.invokeMethod("api2",
               arguments: responce_string,
               result: api2)
        replacedRequest(NSMutableURLRequest())
    }
    
   
}
