#import "GeetestCaptchaFlutterPlugin.h"
#if __has_include(<geetest_captcha_flutter/geetest_captcha_flutter-Swift.h>)
#import <geetest_captcha_flutter/geetest_captcha_flutter-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "geetest_captcha_flutter-Swift.h"
#endif

@implementation GeetestCaptchaFlutterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftGeetestCaptchaFlutterPlugin registerWithRegistrar:registrar];
}
@end
