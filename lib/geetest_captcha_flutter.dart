import 'dart:async';

import 'package:flutter/services.dart';

class GeetestCaptchaFlutter {
  static const MethodChannel _channel =
      const MethodChannel('geetest_captcha_flutter');
  static Future<bool> Function(String)? _onRequest;

  static void initCallHandler() {
    _channel.setMethodCallHandler((MethodCall methodCall) async {
      switch (methodCall.method) {
        case 'api2':
          if (_onRequest == null) return false;
          return await _onRequest!(methodCall.arguments as String);
        default:
          throw MissingPluginException('notImplemented');
      }
    });
  }

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<void> initGeetest({
    required String api1,
    required String authToken,
    required Future<bool> Function(String) onRequest,
  }) async {
    GeetestCaptchaFlutter._onRequest = onRequest;
    await _channel.invokeMethod('initGeetest', {
      'api1': api1,
      'authToken': authToken,
    });
    return;
  }
}